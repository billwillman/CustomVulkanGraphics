#pragma once

#include <vector>
#include "vulkan/vulkan.h"
#include "GLFW/glfw3.h"

// 抽象应用框架
class VkAbstractApplication
{
public:
	// 初始化APP
	virtual bool InitApp() = 0;
	// 运行
	virtual bool Run() = 0;
	// 退出
	virtual void Quit() = 0;
};

// 简单框架，用于软光栅
class VkSimpleApplication : VkAbstractApplication
{
private:
	VkInstance m_Instance;
	VkPhysicalDevice m_PhsicalDevice;
	// 逻辑设备
	VkDevice m_Device;
	VkQueue m_Queue;
	VkSurfaceKHR m_FrontSurface;
	GLFWwindow* m_Window;
	VkSwapchainKHR m_swapChain;
	std::vector<VkImageView> m_ImageViews;
	// 内存池，NULL就是用默认NEW, DELETE
	VkAllocationCallbacks* m_Allocator;
private:
	bool InitWindow();
	bool InitPhysicDevice();
	// 打印扩展信息
	void PrintExtends();
	bool InitSwapChain();
public:
	VkSimpleApplication();

	bool InitApp();
	bool Run();
	void Quit();
};